import React, { Component } from "react";
import Loader from "react-loader-spinner";
import axios from "axios";
class Seed extends Component {
  state = {
    name: "",
    description: "",
    stock: "",
    price: "",
    user_id: this.props.match.params.id,
    seed_id: 0,
    commodity_id: 0,
    commodity: [],
    file: [],
    succSub: false,
    loading: false,
    upload: false,
    errUpload: false,
    error: false
  };

  handleChange = event => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  handleSubmit = event => {
    const {
      name,
      description,
      stock,
      price,
      user_id,
      commodity_id
    } = this.state;
    this.setState({ loading: true });
    event.preventDefault();
    console.log(user_id);
    axios
      .post("http://seedplan-api.herokuapp.com/seeds", {
        name,
        description,
        stock,
        price,
        user_id,
        commodity_id
      })
      .then(res => {
        const { data } = res.data;
        console.log(data);
        this.setState({
          loading: false,
          seed_id: data.id,
          awal: false,
          succSub: true
        });
      })
      .catch(err => {
        console.log(err.response);
        this.setState({ loading: false });
      });
  };

  handleImage = e => {
    this.setState({ upload: true });
    const formData = new FormData();
    formData.append("photo", e.target.files[0]);

    // console.log(formData);
    axios
      .post(
        "http://seedplan-api.herokuapp.com/seeds/" +
          this.state.seed_id +
          "/attachment",
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data"
          }
        }
      )
      .then(res => {
        this.setState({ errUpload: false, upload: false });
      })
      .catch(err => {
        console.log(err.response);
        this.setState({ upload: false, errUpload: true });
      });
  };

  componentDidMount = () => {
    this.setState({ loading: true });
    axios
      .get("http://seedplan-api.herokuapp.com/commodities")
      .then(res => {
        const { data } = res.data;
        this.setState({ commodity: data, loading: false });
      })
      .catch(err => {
        console.log(err.response);
        this.setState({ loading: false });
      });
  };

  render() {
    const {
      name,
      description,
      stock,
      price,
      loading,
      commodity_id,
      commodity,
      upload,
      errUpload,
      succSub
    } = this.state;

    if (loading) {
      return (
        <Loader type="Ball-Triangle" color="green" height={80} width={80} />
      );
    }
    return (
      <React.Fragment>
        {!succSub ? (
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label htmlFor="name">Bibit Tanaman</label>
              <input
                type="text"
                className="form-control"
                id="name"
                name="name"
                value={name}
                onChange={this.handleChange}
                required
                placeholder="Masukkan nama bibit tanaman"
              />
            </div>

            <div className="form-group">
              <label htmlFor="description">Deskripsi</label>
              <textarea
                className="form-control"
                rows="5"
                id="description"
                name="description"
                value={description}
                onChange={this.handleChange}
                required
                placeholder="Masukkan deskripsi tanaman"
              />
            </div>

            <div className="form-group">
              <label htmlFor="stock">Jumlah Stock Bibit</label>
              <input
                type="number"
                className="form-control"
                id="stock"
                name="stock"
                value={stock}
                onChange={this.handleChange}
                required
                placeholder="Masukkan jumlah..."
              />
            </div>
            <div className="form-group">
              <label htmlFor="price">Harga Tiap Bibit</label>
              <input
                type="number"
                className="form-control"
                id="price"
                name="price"
                value={price}
                onChange={this.handleChange}
                required
                placeholder="Masukkan harga..."
              />
            </div>

            <div className="form-group">
              <label htmlFor="commodity">Pilih Komoditas</label>
              <select
                className="form-control"
                name="commodity_id"
                onChange={this.handleChange}
                value={commodity_id}
                required
              >
                <option value="">---Pilih---</option>
                {commodity.map(commodity => {
                  return (
                    <option value={commodity.id} key={commodity.id}>
                      {commodity.name}
                    </option>
                  );
                })}
              </select>
            </div>

            <button type="submit" className="btn btn-success">
              Simpan
            </button>
          </form>
        ) : upload ? (
          <Loader type="Ball-Triangle" color="green" height={80} width={80} />
        ) : errUpload ? (
          <React.Fragment>
            <div className="alert alert-warning" role="alert">
              Terjadi Kesalahan. Coba ulangi kembali!
            </div>
            {/* <form onSubmit={this.handleSubmitImage}> */}
            <div className="form-group">
              <label htmlFor="gambar">Upload Gambar</label>
              <input
                type="file"
                className="form-control-file"
                id="single"
                onChange={this.handleImage}
              />
            </div>
            {/* </form> */}
          </React.Fragment>
        ) : (
          <React.Fragment>
            {/* <form onSubmit={this.handleSubmitImage}> */}
            <div className="form-group">
              <label htmlFor="gambar">Upload Gambar</label>
              <input
                type="file"
                className="form-control-file"
                id="single"
                onChange={this.handleImage}
              />
            </div>
            {/* </form> */}
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

export default Seed;
