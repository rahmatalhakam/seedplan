import React, { Component } from "react";
import Loader from "react-loader-spinner";
import axios from "axios";

export class TableSeed extends Component {
  state = {};

  render() {
    const { data } = this.props;
    let kolom = data.map(data => {
      const { id, photos, name, description, stock, price, commodity } = data;
      return (
        <tr key={id}>
          <td>
            {photos.map((photo, i) => {
              return (
                <img
                  src={photo.url}
                  alt={name}
                  key={photo.id}
                  height="50px"
                  width="50px"
                />
              );
            })}
          </td>
          <td>{name}</td>
          <td>{description}</td>
          <td>{stock}</td>
          <td>{price}</td>
          <td>{commodity}</td>
        </tr>
      );
    });
    return (
      <table
        className="table table-striped text-center"
        style={{ display: "block", overflowY: "scroll", height: "500px" }}
      >
        <thead>
          <tr>
            <th scope="col">Gambar</th>
            <th scope="col">Nama</th>
            <th scope="col">Deskripsi</th>
            <th scope="col">Jumlah Barang</th>
            <th scope="col">Harga</th>
            <th scope="col" colSpan="2">
              Kategori
            </th>
          </tr>
        </thead>
        <tbody>{kolom}</tbody>
      </table>
    );
  }
}

class DataSeed extends Component {
  state = {
    data: [],
    loading: false
  };

  componentDidMount() {
    this.setState({ loading: true });
    axios
      .get(
        "http://seedplan-api.herokuapp.com/seeds?user_id=" +
          this.props.match.params.id +
          ""
      )
      .then(res => {
        const { data } = res.data;
        this.setState({ data, loading: false });
      })
      .catch(err => {
        console.log(err.response);
        this.setState({ loading: false });
      });
  }
  render() {
    const { loading, data } = this.state;

    return (
      <div>
        {loading ? (
          <Loader type="Ball-Triangle" color="green" height={80} width={80} />
        ) : (
          <TableSeed data={data} />
        )}
      </div>
    );
  }
}

export default DataSeed;
