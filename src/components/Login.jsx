import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import Loader from "react-loader-spinner";
import axios from "axios";

class Login extends Component {
  state = {
    id: 0,
    name: "",
    email: "",
    password: "",
    phone_number: "",
    address: "",
    amount: 0,
    error: false,
    loading: false
  };

  handleChange = event => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  handleSubmit = event => {
    event.preventDefault();
    const { email, password } = this.state;
    if (email === "" || password === "") {
      this.setState({ error: true });
    } else {
      this.setState({ loading: true });
      axios
        .post("https://seedplan-api.herokuapp.com/auth/login", {
          email,
          password
        })
        .then(res => {
          const { data } = res.data;
          sessionStorage.setItem("token", data.token);
          sessionStorage.setItem("role", data.role);
          this.setState({ error: false, loading: false });
        })
        .catch(err => {
          console.log(err.response);
          this.setState({ error: true, loading: false });
        });
    }
  };

  render() {
    // error,loading, isLogin
    const { email, password, loading } = this.state;
    const token = sessionStorage.getItem("token");
    const role = sessionStorage.getItem("role");
    if (token && role === "Admin") {
      return <Redirect to="/commodity" />;
    }
    if (loading) {
      return (
        <center>
          <Loader type="Ball-Triangle" color="green" height={80} width={80} />
        </center>
      );
    }
    return (
      <div className="container">
        <div className="card content">
          <div className="card-header" style={{ backgroundColor: "#8CC63F" }}>
            <center>
              <h3 style={{ color: "white" }}>Sign In</h3>
            </center>
          </div>
          <div className="card-body">
            <form onSubmit={this.handleSubmit}>
              <div className="form-group">
                <label htmlFor="email">Email address</label>
                <input
                  type="email"
                  className="form-control"
                  name="email"
                  value={email}
                  onChange={this.handleChange}
                  aria-describedby="email"
                  placeholder="Enter email"
                />
              </div>
              <div className="form-group">
                <label htmlFor="password1">Password</label>
                <input
                  type="password"
                  className="form-control"
                  name="password"
                  value={password}
                  onChange={this.handleChange}
                  placeholder="Password"
                />
              </div>
              <button type="submit" className="btn btn-primary">
                Submit
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
