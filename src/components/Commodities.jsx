import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import axios from "axios";
import Loader from "react-loader-spinner";

export class TableCommodities extends Component {
  state = {};
  render() {
    const { data } = this.props;
    let kolom = data.map(data => {
      const {
        id,
        name,
        low_elevation,
        low_humidity,
        low_ph,
        low_temperature,
        high_elevation,
        high_humidity,
        high_ph,
        high_temperature,
        low_rainfall,
        high_rainfall
      } = data;
      return (
        <tr key={id}>
          <th scope="row">{id}</th>
          <td>{name}</td>
          <td>{low_temperature}</td>
          <td>{high_temperature}</td>
          <td>{low_ph}</td>
          <td>{high_ph}</td>
          <td>{low_humidity}</td>
          <td>{high_humidity}</td>
          <td>{low_elevation}</td>
          <td>{high_elevation}</td>
          <td>{low_rainfall}</td>
          <td>{high_rainfall}</td>
          <td>
            <button className="btn btn-warning btn-block">
              {/** onClick={onEdit} */}
              Edit
            </button>
            <button className="btn btn-danger btn-block">
              {/** onClick={onNonAktif}*/}
              Delete
            </button>
          </td>
        </tr>
      );
    });
    return (
      <table
        className="table table-striped text-center"
        style={{ display: "block", overflowY: "scroll", height: "500px" }}
      >
        <thead>
          <tr>
            <th scope="col" rowSpan="2">
              ID
            </th>
            <th scope="col" rowSpan="2">
              Nama Tumbuhan
            </th>
            <th scope="col" colSpan="2">
              Suhu (Celcius)
            </th>
            <th scope="col" colSpan="2">
              Keasaman Tanah (pH)
            </th>
            <th scope="col" colSpan="2">
              Kelembapan (%)
            </th>
            <th scope="col" colSpan="2">
              Ketinggian Tempat (mdpl)
            </th>
            <th scope="col" colSpan="2">
              Curah Hujan (mm/tahun)
            </th>
            <th scope="col" rowSpan="2">
              Aksi
            </th>
          </tr>
          <tr>
            <th>bawah</th>
            <th>atas</th>
            <th>bawah</th>
            <th>atas</th>
            <th>bawah</th>
            <th>atas</th>
            <th>bawah</th>
            <th>atas</th>
            <th>bawah</th>
            <th>atas</th>
          </tr>
        </thead>
        <tbody>{kolom}</tbody>
      </table>
    );
  }
}

export class FormCommodities extends Component {
  render() {
    const { onSubmit, onChange, data } = this.props;
    return (
      <form onSubmit={onSubmit}>
        <div className="form-group">
          <label htmlFor="name">Nama Tanaman</label>
          <input
            type="text"
            className="form-control"
            id="name"
            name="name"
            value={data.name}
            onChange={onChange}
            required
            placeholder="Masukkan tanaman"
          />
        </div>

        <div className="form-group">
          <label htmlFor="name">Suhu (Celcius)</label>
          <br />
          <div className="row">
            <div className="col">
              <input
                type="number"
                name="low_temperature"
                value={data.low_temperature}
                min="0"
                max="100"
                className="form-control"
                placeholder="Batas bawah"
                onChange={onChange}
              />
            </div>
            <div className="col">
              <input
                type="number"
                name="high_temperature"
                value={data.high_temperature}
                min="0"
                max="100"
                className="form-control"
                placeholder="Batas atas"
                onChange={onChange}
              />
            </div>
          </div>
        </div>

        <div className="form-group">
          <label htmlFor="name">Keasaman Tanah (pH)</label>
          <br />
          <div className="row">
            <div className="col">
              <input
                type="number"
                name="low_ph"
                value={data.low_ph}
                min="0"
                max="14"
                className="form-control"
                placeholder="Batas bawah"
                onChange={onChange}
              />
            </div>
            <div className="col">
              <input
                type="number"
                name="high_ph"
                value={data.high_ph}
                min="0"
                max="14"
                className="form-control"
                placeholder="Batas atas"
                onChange={onChange}
              />
            </div>
          </div>
        </div>

        <div className="form-group">
          <label htmlFor="name">Kelembapan (%)</label>
          <br />
          <div className="row">
            <div className="col">
              <input
                type="number"
                name="low_humidity"
                value={data.low_humidity}
                min="0"
                max="100"
                className="form-control"
                placeholder="Batas bawah"
                onChange={onChange}
              />
            </div>
            <div className="col">
              <input
                type="number"
                name="high_humidity"
                value={data.high_humidity}
                min="0"
                max="100"
                className="form-control"
                placeholder="Batas atas"
                onChange={onChange}
              />
            </div>
          </div>
        </div>

        <div className="form-group">
          <label htmlFor="name">Ketinggian Tempat (mdpl)</label>
          <br />
          <div className="row">
            <div className="col">
              <input
                type="number"
                name="low_elevation"
                value={data.low_elevation}
                min="0"
                max="4000"
                className="form-control"
                placeholder="Batas bawah"
                onChange={onChange}
              />
            </div>
            <div className="col">
              <input
                type="number"
                name="high_elevation"
                value={data.high_elevation}
                min="0"
                max="4000"
                className="form-control"
                placeholder="Batas atas"
                onChange={onChange}
              />
            </div>
          </div>
        </div>

        <div className="form-group">
          <label htmlFor="name">Curah Hujan (mm/tahun)</label>
          <br />
          <div className="row">
            <div className="col">
              <input
                type="number"
                name="low_rainfall"
                value={data.low_rainfall}
                min="0"
                max="5000"
                className="form-control"
                placeholder="Batas bawah"
                onChange={onChange}
              />
            </div>
            <div className="col">
              <input
                type="number"
                name="high_rainfall"
                value={data.high_rainfall}
                min="0"
                max="5000"
                className="form-control"
                placeholder="Batas atas"
                onChange={onChange}
              />
            </div>
          </div>
        </div>

        <button type="submit" className="btn btn-success">
          Simpan
        </button>
      </form>
    );
  }
}
class Coomodities extends Component {
  state = {
    name: "",
    low_temperature: "",
    high_temperature: "",
    low_ph: "",
    high_ph: "",
    low_humidity: "",
    high_humidity: "",
    low_elevation: "",
    high_elevation: "",
    low_rainfall: "",
    high_rainfall: "",
    isLogin: true,
    loading: false,
    error: false,
    data: []
  };

  handleChange = event => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  handleSubmit = event => {
    this.setState({ loading: true });
    event.preventDefault();
    const {
      name,
      low_elevation,
      low_humidity,
      low_ph,
      low_temperature,
      high_elevation,
      high_humidity,
      high_ph,
      high_temperature,
      low_rainfall,
      high_rainfall
    } = this.state;
    console.log(high_elevation);
    axios
      .post(
        "https://seedplan-api.herokuapp.com/commodities",
        {
          name,
          low_elevation,
          low_humidity,
          low_ph,
          low_temperature,
          high_elevation,
          high_humidity,
          high_ph,
          high_temperature,
          low_rainfall,
          high_rainfall
        },
        {
          headers: {
            Authorization: `Bearer ${sessionStorage.getItem("token")}`
          }
        }
      )
      .then(() => {
        axios
          .get("https://seedplan-api.herokuapp.com/commodities")
          .then(res => {
            const { data } = res.data;
            this.setState({ data, loading: false, error: false });
          })
          .catch(err => {
            console.log(err);
            this.setState({ loading: false, error: true });
          });
      })
      .catch(err => {
        console.log(err);
        this.setState({ loading: false, error: true });
      });
  };

  handleLogout = () => {
    sessionStorage.clear();
    this.setState({ isLogin: false });
  };

  componentDidMount() {
    this.setState({ loading: true });
    axios
      .get("https://seedplan-api.herokuapp.com/commodities")
      .then(res => {
        const { data } = res.data;
        this.setState({ data, loading: false, error: false });
      })
      .catch(err => {
        console.log(err.response);
        this.setState({ loading: false, error: true });
      });
  }

  render() {
    const { loading, data, error } = this.state;
    const token = sessionStorage.getItem("token");
    const role = sessionStorage.getItem("role");
    if (!token || !role) {
      return <Redirect to="/" />;
    }
    const errorWarning = () => {
      return (
        <div className="alert alert-warning" role="alert">
          Terjadi Kesalahan. Coba ulangi kembali!
        </div>
      );
    };
    return (
      <React.Fragment>
        <div className="btn btn-danger" onClick={this.handleLogout}>
          Logout
        </div>
        <div className="container">
          <h1>Data Komoditas Tumbuhan Beserta Syarat Tanamnya</h1>
          <div className="container">
            {loading ? (
              <Loader
                type="Ball-Triangle"
                color="green"
                height={80}
                width={80}
              />
            ) : error || data === [] ? (
              errorWarning
            ) : (
              <FormCommodities
                onSubmit={this.handleSubmit}
                onChange={this.handleChange}
                data={this.state}
              />
            )}
          </div>
          <br />
          {loading ? (
            <Loader type="Ball-Triangle" color="green" height={80} width={80} />
          ) : error || data === [] ? (
            errorWarning
          ) : (
            <TableCommodities data={data} />
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default Coomodities;
