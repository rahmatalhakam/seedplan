import React, { Component } from "react";
import Axios from "axios";

export class TableProduk extends Component {
  state = {};

  render() {
    const { data } = this.props;

    let kolom = data.map(data => {
      const {
        id,
        photos,
        name,
        description,
        stock,
        price,
        commodity,
        user
      } = data;
      return (
        <tr key={id}>
          <td>{user.id}</td>
          <td>
            {photos.map((photo, i) => {
              return (
                <img
                  src={photo.url}
                  alt={name}
                  key={photo.id}
                  height="50px"
                  width="50px"
                />
              );
            })}
          </td>
          <td>{name}</td>
          <td>{description}</td>
          <td>{stock}</td>
          <td>{price}</td>
          <td>{commodity}</td>
        </tr>
      );
    });
    return (
      <table
        className="table table-striped text-center"
        style={{ overflowY: "scroll" }}
      >
        <thead>
          <tr>
            <th scope="col">ID Penjual</th>
            <th scope="col">Gambar</th>
            <th scope="col">Nama</th>
            <th scope="col">Deskripsi</th>
            <th scope="col">Jumlah Barang</th>
            <th scope="col">Harga</th>
            <th scope="col">Kategori</th>
          </tr>
        </thead>
        <tbody>{kolom}</tbody>
      </table>
    );
  }
}

class Beli extends Component {
  state = {
    data: [],
    entity: 0,
    delivery_location: ""
  };

  handleChange = event => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  handleSubmit = event => {
    //   event.preventDefault();
    //   const {entity, delivery_location, data} = this.state;
    //   Axios.post("http://seedplan-api.herokuapp.com/orders",{
    //       entity,
    //       delivery_location,
    //       seed_id:
    //   })
  };
  componentDidMount = () => {
    Axios.get(
      "http://seedplan-api.herokuapp.com/seeds?commodity_id=" +
        this.props.location.state
    )
      .then(res => {
        const { data } = res.data;
        this.setState({ data });
      })
      .catch(err => {
        console.log(err.response);
      });
  };

  render() {
    const { data, entity, delivery_location } = this.state;
    console.log(this.props);
    return (
      <div>
        <center>
          <h1>Daftar Penjualan Produk</h1>
        </center>
        <div className="container">
          {data === [] ? null : <TableProduk data={data} />}
        </div>

        <div className="container">
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label htmlFor="name">Jumlah Bibit</label>
              <input
                type="number"
                className="form-control"
                id="entity"
                name="entity"
                value={entity}
                onChange={this.handleChange}
                required
                placeholder="Masukkan jumlah bibit yang ingin dibeli"
              />
            </div>

            <div className="form-group">
              <label htmlFor="description">Tujuan Pengiriman</label>
              <input
                type="text"
                className="form-control"
                id="delivery_location"
                name="delivery_location"
                value={delivery_location}
                onChange={this.handleChange}
                required
                placeholder="Yogyakarta..."
              />
            </div>

            <button type="submit" className="btn btn-success">
              Order Sekarang!
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default Beli;
