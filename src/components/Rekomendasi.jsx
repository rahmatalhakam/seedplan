import React, { Component } from "react";
import Axios from "axios";
import Loader from "react-loader-spinner";
import { Redirect } from "react-router-dom";

export class TableRekomendasi extends Component {
  state = {};
  render() {
    const { hasil, onBeli } = this.props;
    let kolom = hasil.map(data => {
      const { id, name, photo } = data;
      return (
        <tr key={id}>
          <th scope="row">{id}</th>
          <td>
            <img src={photo.url} alt={name} width="70px" height="70px" />
          </td>
          <td>{name}</td>

          <td>
            <button
              className="btn btn-primary btn-block"
              onClick={() => onBeli(id)}
            >
              Beli
            </button>
          </td>
        </tr>
      );
    });
    return (
      <React.Fragment>
        <table
          className="table table-striped text-center"
          style={{ overflowY: "scroll" }}
        >
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Gambar</th>
              <th scope="col">Nama Tumbuhan</th>
              <th scope="col">Beli Bibit</th>

              {/* <th scope="col" rowSpan="2">
              Aksi
            </th> */}
            </tr>
          </thead>
          <tbody>{kolom}</tbody>
        </table>
      </React.Fragment>
    );
  }
}

class Rekomendasi extends Component {
  state = {
    location: 0,
    data: [],
    elevation: "",
    humidity: "",
    tempf: "",
    ph: 0,
    hasil: [],
    loading: false,
    id: 0,
    beli: false,
    buyer_id: this.props.match.params.id
  };

  handleBeli = id => {
    // console.log(id);
    this.setState({ id, beli: true });
  };
  handleChange = event => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
    // const lat = this.state.data[this.state.location].lat;
    // const long = this.state.data[this.state.location].long;

    // Axios.get(
    //   `http://open.mapquestapi.com/elevation/v1/profile?key=u39YV79xHE8CHCaSVRngcApDJhRVF9Yc&latLngCollection=${lat},${long}`
    // )
    //   .then(res => {
    //     this.setState({ elevation: res.elevationProfile[0].height });
    //   })
    //   .catch(err => {
    //     console.log(err);
    //   });
  };

  handleSubmit = event => {
    event.preventDefault();
    this.setState({ loading: true });
    const { ph } = this.state;
    const ID = this.state.data[this.state.location].ID;
    Axios.get(`http://45.126.132.55:4444/monthlydata/?id=${ID}&celcius=True`, {
      headers: {
        Authorization: "Bearer nflc2HjrriiRDolW0gnhTnGqgC1sSV"
      }
    })
      .then(res => {
        Axios.post("https://seedplan-api.herokuapp.com/advice/commodities", {
          ph,
          humidity: res.data[0][0].humidity,
          temperature: res.data[0][0].tempf,
          elevation: 768
        })
          .then(res => {
            const { data } = res.data;
            this.setState({ hasil: data, loading: false });
          })
          .catch(err => {
            console.log(err);
          });
      })
      .then(() => {})
      .catch(err => {
        console.log(err);
      });
  };

  componentDidMount() {
    Axios.get("http://45.126.132.55:4444/sensor/", {
      headers: {
        Authorization: "Bearer nflc2HjrriiRDolW0gnhTnGqgC1sSV"
      }
    })
      .then(res => {
        this.setState({ data: res.data[0] });
      })
      .catch(err => {
        console.log(err);
      });
  }
  render() {
    const {
      location,
      data,
      humidity,
      tempf,
      ph,
      hasil,
      loading,
      beli,
      id,
      buyer_id
    } = this.state;
    if (beli) {
      // return <Redirect to="/commodity" />;
      return (
        <Redirect
          to={{
            pathname: "/beli",
            state: id,
            buyer_id: buyer_id
          }}
        />
      );
    }
    return (
      <div className="container">
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label htmlFor="lokasi">Pilih Lokasi</label>
            <select
              className="form-control"
              name="location"
              onChange={this.handleChange}
              value={location}
              required
            >
              <option value="">---Pilih---</option>
              {data.map((dat, i) => {
                return (
                  <option value={i} key={dat.ID}>
                    {dat.lokasi}
                  </option>
                );
              })}
            </select>
          </div>
          {humidity === "" ? null : (
            <React.Fragment>
              <div className="form-control">
                <label htmlFor="elevation">Kelembapan (%): {humidity}</label>
              </div>

              <div className="form-control">
                <label htmlFor="elevation">Suhu (Celcius) : {tempf}</label>
              </div>
            </React.Fragment>
          )}
          <div className="form-group">
            <label htmlFor="price">Tingkat Keasaman Tanah (pH)</label>
            <input
              type="number"
              className="form-control"
              id="ph"
              name="ph"
              value={ph}
              onChange={this.handleChange}
              required
              placeholder="Masukkan pH..."
            />
          </div>
          <button type="submit" className="btn btn-success">
            Simpan
          </button>
        </form>

        {hasil === [] ? null : loading ? (
          <Loader type="Ball-Triangle" color="green" height={80} width={80} />
        ) : (
          <TableRekomendasi hasil={hasil} onBeli={this.handleBeli} />
        )}
      </div>
    );
  }
}

export default Rekomendasi;
