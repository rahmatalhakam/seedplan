import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Login from "./components/Login";
import NotFound from "./components/NotFound";
import Register from "./components/Register";
import Commodities from "./components/Commodities";
import FormSeed from "./components/dashboard/seed/FormSeed";
import DataSeed from "./components/dashboard/seed/DataSeed";
import Rekomendasi from "./components/Rekomendasi";
import Beli from "./components/Beli";

const Routes = () => (
  <Router>
    <Switch>
      <Route exact path="/" component={Login} />
      <Route path="/register" component={Register} />
      <Route path="/commodity" component={Commodities} />
      <Route path="/beli" component={Beli} />
      <Route path="/seed/:id/create" component={FormSeed} />
      <Route path="/seed/:id" component={DataSeed} />
      <Route path="/rekomendasi/:id" component={Rekomendasi} />
      <Route path="*" component={NotFound} />
    </Switch>
  </Router>
);

export default Routes;
